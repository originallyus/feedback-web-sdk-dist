# AIA Feed Back SDK for Web

DFS Feedback SDK is a javascript library to obtain feedback from user via web browser.

## Installation

- basic jQuery library is required (compatible with all jQuery verions)
- import Javascript for the SDK (you may also manually download this file from our server)

```html
<script src="https://aia-dfs.originally.us/web-p3/feedbackSdk.min.js"></script>
```
- Content-Security-Policy: the following CSP rules are required for the SDK to be working
    - script-src https://*.originally.us/
    - style-src https://*.originally.us/
    - img-src data: https://*.originally.us/
    - font-src https://*.originally.us/
    - connect-src https://*.originally.us/

## Usage

This code will show the feedback/rating form depends on configuration from server side.

Please contact us for the App Sec and PackageID specific to your website.

```javascript
//Optional: obtain this from somewhere else in your project
var userId = 'V12345678';

//Optional: tag this to a certain event
var eventTag = 'redeem_event';

//Optional: choose a language (en, th, ms, ta)
var lang = 'en';

//Optional: use an alternative form configured in CMS
var formSlug = "alt_rating_form";

//Optional: provide your own unique Device UUID
var deviceUuid = '0712c60c-c964-4d1e-b043-be8029986d8f';

//Optional: obtain this from somewhere else in your project
var metadata = 'policy_987654321';

AIAFeedbackSDK.Initialize({
    appSec: "xxxxxxxxxxxxxxxx",
    packageId: "xx.xxxxxxxx.xxxxx",
    debug: false,
    language: lang,                 // optional
    userId: userId,                 // optional
    eventTag: eventTag,             // optional
    formSlug: formSlug,             // optional
    deviceUuid: deviceUuid,         // optional
    metadata: metadata,             // optional
}, function(formDidShow) {
    //Optional callback function
    console.log(formDidShow ? "Form was shown" : "No form was shown");
});
```

## Optional Inline Display For Content Usefulness Survey

For the <b>Content Usefulness</b> survey, there is an additional display type named `inline`. If you want to show an inline survey on your web page, add this `<div>` tags to your web document like this:

```javascript
    ...
    <div id="tim-feedback-sdk-inline-view"></div>
    ...

```

<img src="./images/content_usefulness_inline_web.webp" alt="Alt text" width="400">


## Debug vs Production mode

In Debug mode **(debug = true)**, the rating form is always shown, for debugging purpose.

In Production mode **(debug = false)**, the rating form may not be shown all the time, ie. nothing happens when the code is executed. This is not a bug.

The form might only be shown only once per hour, per day, per week, etc.


## Callback Function (optional)

An optional callback function can be provided to capture the outcome of the SDK. This callback function will be triggered for all success & failure senarios, including network error scenarios. Refer to the code sample above.

  - **formDidShow = false** is triggered when there is no form to be shown, this callback function will be triggered almost immediately.
  - **formDidShow = true** is triggered when has been shown and user has finished interacting with it or dismiss/cancel it


## Other Functions

Retrieve the version number of the SDK

**GetVersion()**

```javascript
const feedbackSdkVersion = AIAFeedbackSDK.GetVersion();
console.log("FeedbackSDK Version:", feedbackSdkVersion);
```

Retrieve the internal status whether a Submit action has been done. This function can be called any time. However, it is recommended to be used within callback function.

**GetDidSubmit()**

```javascript
const didSubmit = AIAFeedbackSDK.GetDidSubmit();
console.log("FeedbackSDK Did Submit:", didSubmit);
```

Retrieve the user-selected rating number. This will returns 0 as default when user has not selected any numeric rating, or forms that does not have any numeric selection such as Comment, Poll, External Survey. The valid value is 1-5 for Satisfaction & CES form, 1-10 for NPS form. Do note that this number is updated in real-time as user clicks on the rating numbers.

**GetRatingNumber()**

```javascript
const ratingNumber = AIAFeedbackSDK.GetRatingNumber();
console.log("FeedbackSDK Rating Number:", ratingNumber);
```