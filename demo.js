//Optional: obtain this from somewhere else in your project
var userId = 'V12345678';

//Optional: obtain this from somewhere else in your project
var eventTag = 'redeem_event';

//Optional: obtain this from somewhere else in your project
var deviceUuid = '1d4f37fc-48b2-4b93-8616-65399dbd5db2';

//Optional: obtain this from somewhere else in your project
var metadata = 'policy_987654321';

//Mandatory: please obtain this from Originally US
const appSec = "sVcnpO9ZjAACcTuxxZ0x";
const packageId = "us.originally.feedback.sdk.sample.web";

//Version of the SDK
var showSdkVersion = function() {
    const feedbackSdkVersion = AIAFeedbackSDK.GetVersion();
    console.log("FeedbackSDK Version:", feedbackSdkVersion);
    $("#sdk-version").html(`Version <strong>${feedbackSdkVersion}</strong>`);
}

 
document.addEventListener('DOMContentLoaded', function () {

    showSdkVersion();
    
    document.getElementById("initTrue").addEventListener("click", function() { init(true); });
    document.getElementById("initFalse").addEventListener("click", function() { init(false); });

    document.getElementById("initNPSTrue").addEventListener("click", function() { initNPS(true); });
    document.getElementById("initNPSFalse").addEventListener("click", function() { initNPS(false); });

    document.getElementById("initPollTrue").addEventListener("click", function() { initPoll(true); });
    document.getElementById("initPollFalse").addEventListener("click", function() { initPoll(false); });

    document.getElementById("initCommentTrue").addEventListener("click", function() { initComment(true); });
    document.getElementById("initCommentFalse").addEventListener("click", function() { initComment(false); });

    document.getElementById("initCESTrue").addEventListener("click", function() { initCES(true); });
    document.getElementById("initCESFalse").addEventListener("click", function() { initCES(false); });

    document.getElementById("initLinkTrue").addEventListener("click", function() { initLink(true); });
    document.getElementById("initLinkFalse").addEventListener("click", function() { initLink(false); });

    document.getElementById("initContentUsefulnessTrue").addEventListener("click", function() { initContentUsefulness(true); });
    document.getElementById("initContentUsefulnessFalse").addEventListener("click", function() { initContentUsefulness(false); });

    $('.language-selector').on('change', function() {
        var lang = $(this).val();
        localStorage.setItem('lang', lang);
       
        if (lang === 'ko') {
            $('head').append('<link rel="stylesheet" type="text/css" href="https://spoqa.github.io/spoqa-han-sans/css/SpoqaHanSansNeo.css">');
        }else {
            //$('link[href="https://spoqa.github.io/spoqa-han-sans/css/SpoqaHanSansNeo.css"]').remove();
        }

    });

    if (localStorage.getItem('lang')) {
        $('.language-selector').val(localStorage.getItem('lang'));
        var lang = localStorage.getItem('lang');
        if (lang === 'ko') {
            $('head').append('<link rel="stylesheet" type="text/css" href="https://spoqa.github.io/spoqa-han-sans/css/SpoqaHanSansNeo.css">');
        }else {
            $('link[href="https://spoqa.github.io/spoqa-han-sans/css/SpoqaHanSansNeo.css"]').remove();
        }
    }

});

var init = function(debug) {
    AIAFeedbackSDK.Initialize({
        appSec: appSec,                         // 11 Nov 2024: remove sensitive '𝓼𝓮𝓬𝓻𝓮𝓽' keyword
        packageId: packageId,                   // note: there's a typo in previous versions, both are compatible
        debug: debug,
        eventTag: eventTag,                     // optional
        userId: userId,                         // optional (can be any string, even hash string)
        deviceUuid: deviceUuid,                 // optional
        metadata: metadata,                     // optional (supports simple string only)
        formSlug: "native_rating_form",			// this is an alias for satisfaction-1 in new version
        language: localStorage.getItem('lang') || 'en'
    }, function(didShow) {
        console.log(didShow ? "Form was shown" : "No form was shown");
    });
}

var initNPS = function(debug) {
    AIAFeedbackSDK.Initialize({
        appSec: appSec,                         // 11 Nov 2024: remove sensitive '𝓼𝓮𝓬𝓻𝓮𝓽' keyword
        packageId: packageId,                   // note: there's a typo in previous versions, both are compatible
        debug: debug,
        eventTag: eventTag,                     // optional
        userId: userId,                         // optional (can be any string, even hash string)
        deviceUuid: deviceUuid,                 // optional
        metadata: metadata,                     // optional (supports simple string only)
        formSlug: "nps-1",
        language: localStorage.getItem('lang') || 'en'
    }, function(didShow) {
        console.log(didShow ? "Form was shown" : "No form was shown");
    });
}

var initPoll = function(debug) {
    AIAFeedbackSDK.Initialize({
        appSec: appSec,                         // 11 Nov 2024: remove sensitive '𝓼𝓮𝓬𝓻𝓮𝓽' keyword
        packageId: packageId,                   // note: there's a typo in previous versions, both are compatible
        debug: debug,
        eventTag: eventTag,                     // optional
        userId: userId,                         // optional (can be any string, even hash string)
        deviceUuid: deviceUuid,                 // optional
        metadata: metadata,                     // optional (supports simple string only)
        formSlug: "poll-1",
        language: localStorage.getItem('lang') || 'en'
    }, function(didShow) {
        console.log(didShow ? "Form was shown" : "No form was shown");
    });
}

var initComment = function(debug) {
    AIAFeedbackSDK.Initialize({
        appSec: appSec,                         // 11 Nov 2024: remove sensitive '𝓼𝓮𝓬𝓻𝓮𝓽' keyword
        packageId: packageId,                   // note: there's a typo in previous versions, both are compatible
        debug: debug,
        eventTag: eventTag,                     // optional
        userId: userId,                         // optional (can be any string, even hash string)
        deviceUuid: deviceUuid,                 // optional
        metadata: metadata,                     // optional (supports simple string only)
        formSlug: "comment-1",
        language: localStorage.getItem('lang') || 'en'
    }, function(didShow) {
        console.log(didShow ? "Form was shown" : "No form was shown");
    });
}

var initCES = function(debug) {
    AIAFeedbackSDK.Initialize({
        appSec: appSec,                         // 11 Nov 2024: remove sensitive '𝓼𝓮𝓬𝓻𝓮𝓽' keyword
        packageId: packageId,                   // note: there's a typo in previous versions, both are compatible
        debug: debug,
        eventTag: eventTag,                     // optional
        userId: userId,                         // optional (can be any string, even hash string)
        deviceUuid: deviceUuid,                 // optional
        metadata: metadata,                     // optional (supports simple string only)
        formSlug: "effort-1",
        language: localStorage.getItem('lang') || 'en'
    }, function(didShow) {
        console.log(didShow ? "Form was shown" : "No form was shown");
    });
}

var initLink = function(debug) {
    AIAFeedbackSDK.Initialize({
        appSec: appSec,                         // 11 Nov 2024: remove sensitive '𝓼𝓮𝓬𝓻𝓮𝓽' keyword
        packageId: packageId,                   // note: there's a typo in previous versions, both are compatible
        debug: debug,
        eventTag: eventTag,                     // optional
        userId: userId,                         // optional (can be any string, even hash string)
        deviceUuid: deviceUuid,                 // optional
        metadata: metadata,                     // optional (supports simple string only)
        formSlug: "external-1",
        language: localStorage.getItem('lang') || 'en'
    }, function(didShow) {
        console.log(didShow ? "Form was shown" : "No form was shown");
    });
}

var initContentUsefulness = function(debug) {
    AIAFeedbackSDK.Initialize({
        appSec: appSec,                         // 11 Nov 2024: remove sensitive '𝓼𝓮𝓬𝓻𝓮𝓽' keyword
        packageId: packageId,                   // note: there's a typo in previous versions, both are compatible
        debug: debug,
        eventTag: eventTag,                     // optional
        userId: userId,                         // optional (can be any string, even hash string)
        deviceUuid: deviceUuid,                 // optional
        metadata: metadata,                     // optional (supports simple string only)
        formSlug: "content_usefulness-2",
        language: localStorage.getItem('lang') || 'en'
    }, function(didShow) {
        console.log(didShow ? "Form was shown" : "No form was shown");
    });
}